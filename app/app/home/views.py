# coding:utf8
import datetime
import os
import uuid
from functools import wraps

from flask import render_template, redirect, url_for, flash, request
from werkzeug.security import generate_password_hash
from werkzeug.utils import secure_filename

from app import app
from app.models import User, Userlog, Preview, Tag, Comment, Movie, Moviecol
from app.models import db
from . import home
from .forms import RegistForm, LoginForm, UserdetailForm, PwdForm, CommentForm

exist = {}


# 修改文件名称
def change_filename(filename):
    fileinfo = os.path.splitext(filename)
    filename = datetime.datetime.now().strftime("%Y%m%d%H%M%S") + str(uuid.uuid4().hex) + fileinfo[-1]
    return filename


def user_login_req(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'user' not in exist:
            return redirect(url_for('home.login', next=request.url))
        return f(*args, **kwargs)

    return decorated_function


@home.route("/layout")
def layout():
    render_template("home/layout.html", exist=exist)


@home.route("/<int:page>/", methods=['GET'])
def index(page=None):
    if page is None:
        page = 1
    tags = Tag.query.all()
    page_data = Movie.query
    # 标签筛选
    tid = request.args.get("tid", 0)
    if int(tid) != 0:
        page_data = page_data.filter_by(
            tag_id=int(tid)
        )
    # 星级筛选
    star = request.args.get("star", 0)
    if int(star) != 0:
        page_data = page_data.filter_by(
            star=int(star)
        )
    # 时间筛选
    time = request.args.get("time", 0)
    if int(time) != 0:
        if int(time) == 1:
            page_data = page_data.order_by(
                Movie.addtime.desc()
            )
        else:
            page_data = page_data.order_by(
                Movie.addtime.asc()
            )
    # 播放量筛选
    pm = request.args.get("pm", 0)
    if int(pm) != 0:
        if int(pm) == 1:
            page_data = page_data.order_by(
                Movie.playnum.desc()
            )
        else:
            page_data = page_data.order_by(
                Movie.playnum.asc()
            )
    # 评论数量筛选
    cm = request.args.get("cm", 0)
    if int(cm) != 0:
        if int(cm) == 1:
            page_data = page_data.order_by(
                Movie.commentnum.desc()
            )
        else:
            page_data = page_data.order_by(
                Movie.commentnum.asc()
            )

    page_data = page_data.paginate(page=page, per_page=10)

    p = dict(
        tid=tid,
        star=star,
        time=time,
        pm=pm,
        cm=cm
    )
    return render_template("home/index.html", tags=tags, p=p, page_data=page_data, exist=exist)


@home.route("/login", methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        data = form.data
        user = User.query.filter_by(name=data['name']).first()
        if not user.check_pwd(data['pwd']):
            flash("密码错误", 'error')
            return redirect(url_for("home.login"))
        exist['user'] = user.name
        exist["user_id"] = user.id
        userlog = Userlog(
            user_id=exist['user_id'],
            ip=request.remote_addr
        )
        db.session.add(userlog)
        db.session.commit()
        return redirect(url_for("home.index", page=1))
    return render_template("home/login.html", form=form)


@home.route("/logout")
def logout():
    exist.pop('user', None)
    exist.pop('user_id', None)
    return redirect(url_for("home.index", page=1))


# 会员注册
@home.route("/regist", methods=['GET', 'POST'])
def regist():
    form = RegistForm()
    if form.validate_on_submit():
        data = form.data
        # 验证注册时两次输入的密码是否相同
        if data['pwd'] != data['re_pwd']:
            flash("两次输入的密码不相同", 'error')
        elif data['pwd'] == data['re_pwd']:
            user = User(
                name=data['name'],
                email=data['email'],
                phone=data['phone'],
                pwd=generate_password_hash(data["pwd"]),
                uuid=uuid.uuid4().hex
            )
            db.session.add(user)
            db.session.commit()
            flash("注册成功！点击返回登录页面...", 'success')
    return render_template("home/regist.html", form=form)


# 会员中心
@home.route("/user", methods=['GET', 'POST'])
@user_login_req
def user():
    form = UserdetailForm()
    user = User.query.get(exist['user_id'])
    form.face.validators = []
    if request.method == 'GET':
        form.name.data = user.name
        form.email.data = user.email
        form.phone.data = user.phone
        form.info.data = user.info
    if form.validate_on_submit():
        data = form.data
        file_face = secure_filename(form.face.data.filename)
        if not os.path.exists(app.config["FC_DIR"]):
            os.makedirs(app.config["FC_DIR"])
            os.chmod(app.config["FC_DIR"], "rw")
        user.face = change_filename(file_face)
        form.face.data.save(app.config["FC_DIR"] + user.face)
        name_count = User.query.filter_by(name=data['name']).count()
        if name_count == 1 and data['name'] != user.name:
            flash("昵称已经存在！", 'error')
        email_count = User.query.filter_by(email=data['email']).count()
        if email_count == 1 and data['email'] != user.email:
            flash("邮箱已经存在！", 'error')
        phone_count = User.query.filter_by(phone=data['phone']).count()
        if phone_count == 1 and data['phone'] != user.phone:
            flash("手机号码已经存在！", 'error')
        user.name = data['name']
        user.email = data['email']
        user.phone = data['phone']
        user.info = data['info']

        db.session.add(user)
        db.session.commit()
        flash("修改成功", 'success')
        return redirect(url_for("home.user"))

    return render_template("home/user.html", form=form, user_data=user)


@home.route("/pwd", methods=['GET', 'POST'])
@user_login_req
def pwd():
    form = PwdForm()
    if form.validate_on_submit():
        data = form.data
        user = User.query.filter_by(name=exist['user']).first()
        from werkzeug.security import generate_password_hash
        user.pwd = generate_password_hash(data["new_pwd"])
        db.session.add(user)
        db.session.commit()
        flash("修改密码成功!请重新登陆！", 'success')
        return redirect(url_for("home.login"))
    return render_template("home/pwd.html", form=form)


@home.route("/comments/<int:page>", methods=['GET', 'POST'])
@user_login_req
def comments(page=None):
    if page is None:
        page = 1
    page_data = Comment.query.filter_by(
        user_id=int(exist['user_id'])
    ).order_by(
        Comment.addtime.desc()
    ).paginate(page=page, per_page=8)
    com_data = Comment.query.filter_by(user_id=int(exist['user_id']))

    for v in com_data:
        user_data = User.query.filter_by(id=int(exist['user_id']))
        movie_data = Movie.query.filter_by(id=int(v.movie_id))
    return render_template("home/comments.html", page_data=page_data, user_data=user_data, movie_data=movie_data)


# 会员登陆日志
@home.route("/loginlog/<int:page>/", methods=['GET', 'POST'])
@user_login_req
def loginlog(page=None):
    if page is None:
        page = 1
    page_data = Userlog.query.filter_by(
        user_id=int(exist['user_id'])
    ).order_by(
        Userlog.addtime.desc()
    ).paginate(page=page, per_page=8)

    return render_template("home/loginlog.html", page_data=page_data)


# 添加收藏
@home.route("/moviecol/add/", methods=['GET'])
@user_login_req
def moviecol_add():
    uid = request.args.get("uid", "")
    mid = request.args.get("mid", "")
    moviecol = Moviecol.query.filter_by(
        user_id=int(uid),
        movie_id=int(mid)
    ).count()
    if moviecol == 1:
        data = dict(ok=0)
    else:
        moviecol = Moviecol(
            user_id=int(uid),
            movie_id=int(mid)
        )
        db.session.add(moviecol)
        db.session.commit()
        data = dict(ok=1)
    import json
    return json.dumps(data)


# 电影收藏
@home.route("/moviecol/<int:page>/")
@user_login_req
def moviecol(page):
    if page is None:
        page = 1
    page_data = Moviecol.query.join(Movie).join(User).filter(
        Movie.id==Moviecol.movie_id,User.id==exist['user_id']
    ).order_by(
        Moviecol.addtime.desc()
    ).paginate(page=page, per_page=8)
    return render_template("home/moviecol.html",page_data=page_data)


@home.route("/animation")
def animation():
    data = Preview.query.all()
    length = len(data)
    return render_template("home/animation.html", data=data, len=length)


# 搜索电影
@home.route("/search/<int:page>", methods=['GET', 'POST'])
def search(page=None):
    if page is None:
        page = 1
    key = request.args.get("key", "")
    movie_count = Movie.query.filter(
        Movie.title.ilike('%' + key + '%')
    ).count()
    page_data = Movie.query.filter(
        Movie.title.ilike('%' + key + '%')
    ).order_by(
        Movie.addtime.desc()
    ).paginate(page=page, per_page=8)
    return render_template("home/search.html", key=key, page_data=page_data, movie_count=movie_count)


@home.route("/play/<int:id>/<int:page>", methods=['GET', 'POST'])
@user_login_req
def play(id=None, page=None):
    movie_comment_num = Comment.query.join(
        Movie
    ).filter(
        Movie.id == int(id),
    ).count()
    movie = Movie.query.join(Tag).filter(
        Tag.id == Movie.tag_id,
        Movie.id == int(id)
    ).first_or_404()
    if page is None:
        page = 1
    page_data = Comment.query.join(
        Movie
    ).join(
        User
    ).filter(
        Movie.id == movie.id,
        User.id == Comment.user_id
    ).order_by(
        Comment.addtime.desc()
    ).paginate(page=page, per_page=10)
    movie.playnum = movie.playnum + 1
    form = CommentForm()
    if form.validate_on_submit():
        data = form.data
        comment = Comment(
            content=data['content'],
            movie_id=movie.id,
            user_id=exist['user_id']
        )
        db.session.add(comment)
        db.session.commit()
        flash("评论成功", 'success')
        return redirect(url_for('home.play', id=movie.id, page=1))
    db.session.add(movie)
    db.session.commit()
    return render_template("home/play.html", movie=movie, form=form, page_data=page_data, exist=exist,
                           num=movie_comment_num)
