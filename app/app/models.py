# coding:utf8

from datetime import datetime

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import check_password_hash

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "mysql+cymysql://root:xllwanysys.@127.0.0.1:3306/movie"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
app.config["SECRET_KEY"] = '14c1524b67fe4bb2b3557adce8b929fa'

db = SQLAlchemy(app)


class User(db.Model):
    __tablename__ = "user"
    # __table_args__ = {"extend_existing": True}
    # 会员编号
    id = db.Column(db.Integer, primary_key=True)
    # 会员昵称
    name = db.Column(db.String(255), unique=True)
    # 密码
    pwd = db.Column(db.String(100))
    # 会员邮箱
    email = db.Column(db.String(100), unique=True)
    # 会员电话
    phone = db.Column(db.String(11))
    # 会员简介
    info = db.Column(db.Text)
    # 会员头像
    face = db.Column(db.String(255), unique=True)
    # 注册时间
    addtime = db.Column(db.DateTime, index=True, default=datetime.now)
    # 唯一标识符
    uuid = db.Column(db.String(255), unique=True)
    # 会员登陆日志外键关系关联
    userlogs = db.relationship("Userlog", backref="user")
    # 评论外键关系关联
    comments = db.relationship("Comment", backref="user")
    # 收藏外键关系关联
    moviecols = db.relationship("Moviecol", backref="user")

    def __repr__(self):
        return "<User %r>" % self.name

    def check_pwd(self,pwd):
        from werkzeug.security import check_password_hash
        return check_password_hash(self.pwd,pwd)

# 会员登陆日志
class Userlog(db.Model):
    __tablename__ = "userlog"
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    ip = db.Column(db.String(100))
    addtime = db.Column(db.DateTime, index=True, default=datetime.now)

    def __repr__(self):
        return "<Userlog %r>" % self.id


# 标签
class Tag(db.Model):
    __tablename__ = "tag"
    # __table_args__ = {"extend_existing": True}
    # 编号
    id = db.Column(db.Integer, primary_key=True)
    # 标题
    name = db.Column(db.String(255), unique=True)
    # # 添加时间
    addtime = db.Column(db.String(255), unique=True, default=datetime.now)
    # 电影外键关联
    movies = db.relationship("Movie", backref='tag')

    def __repr__(self):
        return "<Tag %r>" % self.name


# 电影
class Movie(db.Model):
    __tablename__ = "movie"
    # __table_args__ = {"extend_existing": True}
    # 编号
    id = db.Column(db.Integer, primary_key=True)
    # 标题
    title = db.Column(db.String(255), unique=True)
    # 地址
    url = db.Column(db.String(255), unique=True)
    # 简介
    info = db.Column(db.Text)
    # 封面
    logo = db.Column(db.String(255), unique=True)
    # 星级
    star = db.Column(db.SmallInteger)
    # 播放量
    playnum = db.Column(db.BigInteger)
    # 评论量
    commentnum = db.Column(db.BigInteger)
    # 所属标签
    tag_id = db.Column(db.Integer, db.ForeignKey('tag.id'))
    # 上映地区
    area = db.Column(db.String(255))
    # 上映时间
    release_time = db.Column(db.Date)
    # 播放时间
    length = db.Column(db.String(100))
    # 添加时间
    addtime = db.Column(db.DateTime, index=True, default=datetime.now)
    # 评论外键关系关联
    comments = db.relationship("Comment", backref='movie')
    # 收藏外键关系关联
    moviecols = db.relationship("Moviecol", backref='movie')

    def __repr__(self):
        return "<Movie %r>" % self.title


# 上映预告
class Preview(db.Model):
    __tablename__ = "preview"
    # __table_args__ = {"extend_existing": True}
    # 编号
    id = db.Column(db.Integer, primary_key=True)
    # 标题
    title = db.Column(db.String(255), unique=True)
    # 封面
    logo = db.Column(db.String(255), unique=True)
    # 添加时间
    addtime = db.Column(db.DateTime, index=True, default=datetime.now)

    def __repr__(self):
        return "<Preview %r>" % self.title


# 评论
class Comment(db.Model):
    __tablename__ = "comment"
    # __table_args__ = {"extend_existing": True}
    # 编号
    id = db.Column(db.Integer, primary_key=True)
    # 评论内容
    content = db.Column(db.Text)
    # 所属电影
    movie_id = db.Column(db.Integer, db.ForeignKey('movie.id'))
    # 所属用户
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    # 添加时间
    addtime = db.Column(db.DateTime, index=True, default=datetime.now)

    def __repr__(self):
        return "<Comment %r>" % self.id


# 电影收藏
class Moviecol(db.Model):
    __tablename__ = "moviecol"
    # 编号
    id = db.Column(db.Integer, primary_key=True)
    # 所属电影
    movie_id = db.Column(db.Integer, db.ForeignKey('movie.id'))
    # 所属用户
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    # 添加时间
    addtime = db.Column(db.DateTime, index=True, default=datetime.now)

    def __repr__(self):
        return "<Moviecol %r>" % self.id


# 权限
class Auth(db.Model):
    __tablename__ = "auth"
    # __table_args__ = {"extend_existing": True}
    # 编号
    id = db.Column(db.Integer, primary_key=True)
    # 名称
    name = db.Column(db.String(100), unique=True)
    # 地址
    url = db.Column(db.String(255), unique=True)
    # 添加时间
    addtime = db.Column(db.DateTime, index=True, default=datetime.now)

    def __repr__(self):
        return "<Auth %r>" % self.name


# 角色
class Role(db.Model):
    __tablename__ = "role"
    # __table_args__ = {"extend_existing": True}
    # 编号
    id = db.Column(db.Integer, primary_key=True)
    # 名称
    name = db.Column(db.String(100), unique=True)
    # 权限
    auths = db.Column(db.String(600))
    # 添加时间
    addtime = db.Column(db.DateTime, index=True, default=datetime.now)
    # 管理员外键关系关联
    admins = db.relationship("Admin", backref="role")

    def __repr__(self):
        return "<Role %r>" % self.name


# 管理员
class Admin(db.Model):
    __tablename__ = "admin"
    # __table_args__ = {"extend_existing": True}
    # 管理员编号
    id = db.Column(db.Integer, primary_key=True)
    # 管理员昵称
    name = db.Column(db.String(100), unique=True)
    # 管理员密码
    pwd = db.Column(db.String(100))
    # 是否为超级管理员
    is_super = db.Column(db.SmallInteger)  # 1为是，2为否
    # 所属角色
    role_id = db.Column(db.Integer, db.ForeignKey('role.id'))
    # 添加时间
    addtime = db.Column(db.DateTime, index=True, default=datetime.now)
    # 管理员登陆日志外键关系关联
    adminlogs = db.relationship("Adminlog", backref="admin")
    # 管理员操作日志外键关系关联
    oplogs = db.relationship("Oplog", backref="admin")

    def __repr__(self):
        return "<Admin %r>" % self.name

    # def pwd_hash(self,pwd):
    #     pwd_hash = generate_password_hash(pwd)

    def check_pwd(self, pwd):
        return check_password_hash(self.pwd, pwd)


# 管理员登陆日志
class Adminlog(db.Model):
    __tablename__ = "adminlog"
    # __table_args__ = {"extend_existing": True}
    # 编号
    id = db.Column(db.Integer, primary_key=True)
    # 所属管理员
    admin_id = db.Column(db.Integer, db.ForeignKey("admin.id"))
    # 登录IP
    ip = db.Column(db.String(100))
    # 添加时间
    addtime = db.Column(db.DateTime, index=True, default=datetime.now)

    def __repr__(self):
        return "<Adminlog %r>" % self.id


# 操作日志
class Oplog(db.Model):
    __tablename__ = "oplog"
    # __table_args__ = {"extend_existing": True}
    # 编号
    id = db.Column(db.Integer, primary_key=True)
    # 所属管理员
    admin_id = db.Column(db.Integer, db.ForeignKey("admin.id"))
    # 登录IP
    ip = db.Column(db.String(100))
    # 操作原因
    reason = db.Column(db.String(600))
    # 添加时间
    addtime = db.Column(db.DateTime, index=True, default=datetime.now)

    def __repr__(self):
        return "<Oplog %r>" % self.id


#
if __name__ == '__main__':
    db.create_all()
    # from werkzeug.security import generate_password_hash
    #
    # a = Admin(id="2",
    #          name="沐沐",
    #          pwd=generate_password_hash("mumumumu"))
    # db.session.add(a)
    # db.session.commit()
